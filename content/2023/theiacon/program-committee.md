---
title: Program Committee
seo_title: Program Committee | Theiacon 2023
date: 2023-09-14T09:07:24-04:00
description: The program committee for Theiacon 2023
categories: []
keywords: ['theiacon', '2023', 'program committee']
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: single
---

{{< events/user_bios event="theiacon" year="2023" source="committee" imgRoot="/2023/theiacon/images/committee/" >}}
