---
title: "Eclipse SAAM on Cloud-to-Edge Continuum 2023"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2023 Registration</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        3rd Eclipse Security, AI, Architecture and Modelling Conference<br/>on Cloud to Edge Continuum
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center" style="color: white;">October 17, 2023 | Ludwigsburg, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2023-10-17T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-esaam2023-event"
hide_breadcrumb: true
show_featured_footer: false
hide_call_for_action: true
main_menu: esaam2023  
container: "container-fluid esaam-2023-event"
summary: "eSAAM 2023 will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, specifically  focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2023"
layout: single
keywords: ["eclipse", "UOM", "UPM", "conference", "research", "eSAAM", "SAAM", "Cloud", "Edge", "IoT", "Cloud-Edge-Continuum", "Edge-Cloud-Continuum", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling", "Registration"]
links: [ [href: "..", text: "Main Page"], [href: "https://eclipsecon.regfox.com/eclipsecon-2023", text: "Register Now"] ]
draft: false
---

{{< grid/section-container id="things_to_know" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
	{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
	{{< grid/div class="col-md-16 padding-bottom-20" isMarkdown="true">}}

## Things to Know
* The [Registration FAQs page](https://www.eclipsecon.org/2023/registration-faqs) includes information on policies, payments, discounts, transfers, cancellations, etc.
* The paper speaker receives a free conference pass; co-writers do not receive this discount
* All registrants must agree to the  [Code of Conduct](https://www.eclipsecon.org/2023/eclipse-community-events-code-conduct) as part of the registration process.
* Please see [EclipseCon and Your Privacy](https://www.eclipsecon.org/2023/privacy) to learn more on your privacy.
* The conference language is English.
* Lunch is provided during the eSAAM conference.
* Payment must be made via credit card in euros.
* 19% German VAT is applied to all registration prices.
{{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="how" class="font-1 featured-section-row featured-section-row-highligthed-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
	{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
	{{< grid/div class="col-md-16 padding-bottom-20" isMarkdown="true">}}
## How to Register

Registration is done on an external site. Clicking on an orange “Register Now” button will take you to that site. 

**BUT** – before you click, please read the information below about Pass Types and Discounts. Otherwise, you may pay more than you need to, or miss out on registering for something you want to attend!

Please follow these steps once you are at the registration site:

1. Go to the [registration page](https://eclipsecon.regfox.com/eclipsecon-2023)
1. Enter your personal information
1. In the box labeled **Special Access Key**, enter the **[ESAAM](#how)** code to view and benefit from eSAAM rates
1. Select one of the available eSAAM passes
1. If you have a **Coupon Code**, enter it in the appropriate box
1. Answer the rest of the questions on the form and pay if you have a balance due
1. Be sure to click on **REGISTER** at the bottom of the page
1. You will received a confirmation email after your registration is complete

{{</ grid/div >}}
{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}

{{< grid/div class="col-md-9 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
   {{< bootstrap/button linkClass="btn-primary btn-wide" href="https://eclipsecon.regfox.com/eclipsecon-2023">}}Register Now{{</bootstrap/button>}}
{{</ grid/div >}}
	
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="prices" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
	{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
	{{< grid/div class="col-md-16 padding-bottom-20" isMarkdown="true">}}
## Registration Prices
Some of the eSAAM 2023 passes include access to [Community Day](https://www.eclipsecon.org/2023/community-day) and [EclipseCon 2023](https://www.eclipsecon.org/2023). To learn more about these days, please click on the links.

Note that 19% German VAT is applied to all registration.
| Pass Type                                                      | Prices     | 
| :---                                                           |      ----: |
| **eSAAM 2023 Pass** (Tuesday 17)                               | €150 + VAT |
| **eSAAM 2023 + Community Day** (Monday 16 and Tuesday 17)      | €175 + VAT |
| **eSAAM 2023 + EclipseCon 2023** (Tuesday 16 to Thursday 19)   | €350 + VAT |
| **eSAAM 2023 All Access** (Monday 16 to Thursday 19)           | €375 + VAT |
{{</ grid/div >}}
{{< grid/div class="col-md-4 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}

{{< grid/div class="col-md-9 padding-bottom-20" isMarkdown="true">}}{{</ grid/div >}}
{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
   {{< bootstrap/button linkClass="btn-primary btn-wide" href="https://eclipsecon.regfox.com/eclipsecon-2023">}}Register Now{{</bootstrap/button>}}
{{</ grid/div >}}
	
{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="international_travelers" class="featured-section-row featured-section-row-highligthed-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## International Travelers
If you need a letter from us in order to obtain travel documents, please [email us](mailto:research@eclipsecon.org). We can provide a signed letter as an email attachment that confirms your status as an eSAAM 2023 registrant or an eSAAM 2023 speaker. Here is [an example of a letter](2023_Template_Registrant.pdf).
{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}

{{< grid/section-container id="international_travelers" class="featured-section-row featured-section-row-light-bg">}}
{{< grid/div class="row" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20" isMarkdown="true">}}
## Questions
If you have questions or need help with registration, please [email us](mailto:research@eclipsecon.org).

{{</ grid/div >}}

{{</ grid/div >}}
{{</ grid/section-container >}}


