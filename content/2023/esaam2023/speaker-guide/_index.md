---
title: "eSAAM 2023 Speaker Guideline"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2023 Speaker Guideline</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        3rd Eclipse Security, AI, Architecture and Modelling Conference<br/>on Cloud to Edge Continuum
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center" style="color: white;">October 17, 2023 | Ludwigsburg, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2023-10-17T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-esaam2023-event"
hide_breadcrumb: true
show_featured_footer: false
hide_call_for_action: true
main_menu: esaam2023  
container: "container-fluid esaam-2023-event"
summary: "eSAAM 2023 will bring together industry experts and researchers working on innovative software and systems solutions for the next generation of edge-cloud computing continuum, specifically  focusing on Security and Privacy, Artificial Intelligence and Machine Learning, Systems and Software Architecture, Modelling and related challenges. This event is co-located with EclipseCon 2023"
layout: single
keywords: ["eclipse", "UOM", "UPM", "conference", "research", "eSAAM", "SAAM", "Cloud", "Edge", "IoT", "Cloud-Edge-Continuum", "Edge-Cloud-Continuum", "Cloud Computing", "Security", "AI", "Architecture", "Modelling", "Modeling", "Registration"]
links: [ [href: "..", text: "Main Page"], [href: "https://eclipsecon.regfox.com/eclipsecon-2023", text: "Register Now"] ]
draft: false
---
{{< grid/section-container class="featured-section-row" isMarkdown="true">}}

Welcome to the **eSAAM 2023 speaker guide**! We are here to support our presenters and make sure that your experience is positive, stress-free, and as technically smooth as possible. 
Congratulations on having your talk accepted for eSAAM 2023. We are very happy to welcome you for the event co-located with **EclipseCon 2023**.


This guide is an important part of that support. Please be sure to read it before you attend the conference.
As always, if you have questions, comments, or need help, email us at [research@eclipse.org](mailto:research@eclipse.org). We are here to help!

## Schedule 

The [schedule](../#agenda) has been published! Please look for your talk, and let us know ASAP if you have any questions or need help.

## Speaker Registration

Your registration confirms that you will be attending and giving the talk, so [register promptly](../registration)! 
You will or you should receive an email with a discount code to register free of charge at the eSAAM 2023 conference.

## Speaker Discounts
The designated speaker of the accepted paper receives a speaker discount for the conference. He will also get a great discount to join the EclipseCon 2023 event. So don't miss this opportunity to meet the Eclipse community.
Please be sure to select the pass that corresponds to your anticipated attendance so that we have an accurate count for each day.

## Hotel
Be sure to book your hotel, especially if you plan to stay at the Nestor. We expect the Nestor to sell out. For more information, see the [Hotel page](https://www.eclipsecon.org/2023/hotel) on the EclipseCon website.

## Your Photo and Bio
We have a [Speakers page](../speakers) on the conference website. If you didn't do it yet, please don't forget to send them to us.

## Promoting Your Talk
Get more people to your talk! Blog, tweet (#esaam), invite friends and colleagues to attend. 

## Your presentation 
Please send us your presentation the week before the conference (**deadline Friday, October 13**). 
Your presentation will run on a Mac, so a PDF, or a PowerPoint format for your presentation file is welcome.

## Presenting
Each paper presentation session is 20 minutes, including 5 minutes for questions. Please limit your talk to no more than 15 minutes to allow for the 5 minute Q&A session at the end. 
There will be a chairman/moderator for each of the presentations. The moderator will monitor the session, questions and the time.

## Recording Talks
Talks will be recorded with both video and audio, using a professional recording crew. The recordings will later be uploaded to the [Eclipse Foundation YouTube channel](https://www.youtube.com/@EclipseFdn).

## Internet Access
The session room should have a wired line for speaker use. We cannot guarantee the speed or reliability of the attendee WiFi, so please design your talk so that its success does not depend on good attendee connectivity.

{{</ grid/section-container >}}
